ALTER TABLE orders DROP CONSTRAINT fk_order_user;

ALTER TABLE seats DROP CONSTRAINT fk_seat_hall;

ALTER TABLE screenings DROP CONSTRAINT fk_screening_hall;

ALTER TABLE screenings DROP CONSTRAINT fk_screening_film;

ALTER TABLE reservations DROP CONSTRAINT fk_reservation_seat;

ALTER TABLE reservations DROP CONSTRAINT fk_reservation_screening;

ALTER TABLE order_has_reservation DROP CONSTRAINT fk_ohr_order;

ALTER TABLE order_has_reservation DROP CONSTRAINT fk_ohr_reservation;

ALTER TABLE film_has_category DROP CONSTRAINT fk_film_has_category_film;

ALTER TABLE film_has_category DROP CONSTRAINT fk_film_has_category_category;

DROP TABLE halls;
DROP TABLE seats;
DROP TABLE films;
DROP TABLE categories;
DROP TABLE film_has_category;
DROP TABLE screenings;
DROP TABLE reservations;
DROP TABLE orders;
DROP TABLE order_has_reservation;
DROP TABLE users;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE halls (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL
);

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE seats (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  hall_id bigint NOT NULL,
  line int NOT NULL,
  seat int NOT NULL
);


-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE films (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  age_restriction int NOT NULL,
  premiere_datetime datetime(0) NOT NULL,
  duration datetime(0) NOT NULL,
  description longtext
);


-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE categories (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL
);


-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE film_has_category (
  film_id bigint,
  category_id bigint,
  PRIMARY KEY (film_id, category_id)
);

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE screenings (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  film_id bigint NOT NULL,
  hall_id bigint NOT NULL,
  start_time datetime(0) NOT NULL,
  price int NOT NULL
);


-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE reservations (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  seat_id bigint NOT NULL,
  screening_id bigint NOT NULL
);


-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE orders (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  user_id bigint NOT NULL,
  create_time datetime(0) NOT NULL,
  total_price bigint NOT NULL
);


-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE order_has_reservation (
  reservation_id bigint,
  order_id bigint,
  PRIMARY KEY (reservation_id, order_id)
);

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE users (
  id bigint PRIMARY KEY AUTO_INCREMENT,
  email varchar(255) UNIQUE NOT NULL,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL
);


ALTER TABLE orders ADD CONSTRAINT fk_order_user FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE seats ADD CONSTRAINT fk_seat_hall FOREIGN KEY (hall_id) REFERENCES halls (id);

ALTER TABLE screenings ADD CONSTRAINT fk_screening_hall FOREIGN KEY (hall_id) REFERENCES halls (id);

ALTER TABLE screenings ADD CONSTRAINT fk_screening_film FOREIGN KEY (film_id) REFERENCES films (id);

ALTER TABLE reservations ADD CONSTRAINT fk_reservation_seat FOREIGN KEY (seat_id) REFERENCES seats (id);

ALTER TABLE reservations ADD CONSTRAINT fk_reservation_screening FOREIGN KEY (screening_id) REFERENCES screenings (id);

ALTER TABLE order_has_reservation ADD CONSTRAINT fk_ohr_order FOREIGN KEY (order_id) REFERENCES orders (id);

ALTER TABLE order_has_reservation ADD CONSTRAINT fk_ohr_reservation FOREIGN KEY (reservation_id) REFERENCES reservations (id);

ALTER TABLE film_has_category ADD CONSTRAINT fk_film_has_category_film FOREIGN KEY (film_id) REFERENCES films (id);

ALTER TABLE film_has_category ADD CONSTRAINT fk_film_has_category_category FOREIGN KEY (category_id) REFERENCES categories (id);