ALTER TABLE orders DROP CONSTRAINT fk_order_user;

ALTER TABLE seats DROP CONSTRAINT fk_seat_hall;

ALTER TABLE screenings DROP CONSTRAINT fk_screening_hall;

ALTER TABLE screenings DROP CONSTRAINT fk_screening_film;

ALTER TABLE reservations DROP CONSTRAINT fk_reservation_seat;

ALTER TABLE reservations DROP CONSTRAINT fk_reservation_screening;

ALTER TABLE order_has_reservation DROP CONSTRAINT fk_ohr_order;

ALTER TABLE order_has_reservation DROP CONSTRAINT fk_ohr_reservation;

ALTER TABLE film_has_category DROP CONSTRAINT fk_film_has_category_film;

ALTER TABLE film_has_category DROP CONSTRAINT fk_film_has_category_category;

DROP TRIGGER hall_seq_tr;
DROP TRIGGER seat_seq_tr;
DROP TRIGGER film_seq_tr;
DROP TRIGGER category_seq_tr;
DROP TRIGGER screening_seq_tr;
DROP TRIGGER reservation_seq_tr;
DROP TRIGGER order_seq_tr;
DROP TRIGGER user_seq_tr;

DROP SEQUENCE hall_seq;
DROP SEQUENCE seat_seq;
DROP SEQUENCE film_seq;
DROP SEQUENCE category_seq;
DROP SEQUENCE screening_seq;
DROP SEQUENCE reservation_seq;
DROP SEQUENCE order_seq;
DROP SEQUENCE user_seq;

DROP TABLE halls;
DROP TABLE seats;
DROP TABLE films;
DROP TABLE categories;
DROP TABLE film_has_category;
DROP TABLE screenings;
DROP TABLE reservations;
DROP TABLE orders;
DROP TABLE order_has_reservation;
DROP TABLE users;

CREATE TABLE halls (
  id number(10) PRIMARY KEY,
  name varchar2(255) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE hall_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER hall_seq_tr
 BEFORE INSERT ON halls FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT hall_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE seats (
  id number(10) PRIMARY KEY,
  hall_id number(10) NOT NULL,
  line number(10) NOT NULL,
  seat number(10) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE seat_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER seat_seq_tr
 BEFORE INSERT ON seats FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT seat_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE films (
  id number(10) PRIMARY KEY,
  name varchar2(255) NOT NULL,
  age_restriction number(10) NOT NULL,
  premiere_datetime timestamp(0) NOT NULL,
  duration timestamp(0) NOT NULL,
  description clob
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE film_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER film_seq_tr
 BEFORE INSERT ON films FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT film_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE categories (
  id number(10) PRIMARY KEY,
  name varchar2(255) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE category_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER category_seq_tr
 BEFORE INSERT ON categories FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT category_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE film_has_category (
  film_id number(10),
  category_id number(10),
  PRIMARY KEY (film_id, category_id)
);

CREATE TABLE screenings (
  id number(10) PRIMARY KEY,
  film_id number(10) NOT NULL,
  hall_id number(10) NOT NULL,
  start_time timestamp(0) NOT NULL,
  price number(10) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE screening_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER screening_seq_tr
 BEFORE INSERT ON screenings FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT screening_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE reservations (
  id number(10) PRIMARY KEY,
  seat_id number(10) NOT NULL,
  screening_id number(10) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE reservation_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER reservation_seq_tr
 BEFORE INSERT ON reservations FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT reservation_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE orders (
  id number(10) PRIMARY KEY,
  user_id number(10) NOT NULL,
  create_time timestamp(0) NOT NULL,
  total_price number(10) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE order_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER order_seq_tr
 BEFORE INSERT ON orders FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT order_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

CREATE TABLE order_has_reservation (
  reservation_id number(10),
  order_id number(10),
  PRIMARY KEY (reservation_id, order_id)
);

CREATE TABLE users (
  id number(10) PRIMARY KEY,
  email varchar2(255) UNIQUE NOT NULL,
  username varchar2(255) NOT NULL,
  password varchar2(255) NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE user_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER user_seq_tr
 BEFORE INSERT ON users FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT user_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/

ALTER TABLE orders ADD CONSTRAINT fk_order_user FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE seats ADD CONSTRAINT fk_seat_hall FOREIGN KEY (hall_id) REFERENCES halls (id);

ALTER TABLE screenings ADD CONSTRAINT fk_screening_hall FOREIGN KEY (hall_id) REFERENCES halls (id);

ALTER TABLE screenings ADD CONSTRAINT fk_screening_film FOREIGN KEY (film_id) REFERENCES films (id);

ALTER TABLE reservations ADD CONSTRAINT fk_reservation_seat FOREIGN KEY (seat_id) REFERENCES seats (id);

ALTER TABLE reservations ADD CONSTRAINT fk_reservation_screening FOREIGN KEY (screening_id) REFERENCES screenings (id);

ALTER TABLE order_has_reservation ADD CONSTRAINT fk_ohr_order FOREIGN KEY (order_id) REFERENCES orders (id);

ALTER TABLE order_has_reservation ADD CONSTRAINT fk_ohr_reservation FOREIGN KEY (reservation_id) REFERENCES reservations (id);

ALTER TABLE film_has_category ADD CONSTRAINT fk_film_has_category_film FOREIGN KEY (film_id) REFERENCES films (id);

ALTER TABLE film_has_category ADD CONSTRAINT fk_film_has_category_category FOREIGN KEY (category_id) REFERENCES categories (id);
