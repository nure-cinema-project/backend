-- Halls
INSERT INTO halls (id, name) VALUES (1, 'Blue');
INSERT INTO halls (id, name) VALUES (2, 'Red');

-- Seats
-- Blue hall is 3x3
INSERT INTO seats (id, hall_id, line, seat) VALUES (1, 1, 1, 1);
INSERT INTO seats (id, hall_id, line, seat) VALUES (2, 1, 1, 2);
INSERT INTO seats (id, hall_id, line, seat) VALUES (3, 1, 1, 3);

INSERT INTO seats (id, hall_id, line, seat) VALUES (4, 1, 2, 1);
INSERT INTO seats (id, hall_id, line, seat) VALUES (5, 1, 2, 2);
INSERT INTO seats (id, hall_id, line, seat) VALUES (6, 1, 2, 3);

INSERT INTO seats (id, hall_id, line, seat) VALUES (7, 1, 3, 1);
INSERT INTO seats (id, hall_id, line, seat) VALUES (8, 1, 3, 2);
INSERT INTO seats (id, hall_id, line, seat) VALUES (9, 1, 3, 3);

-- Red hall is 2x5
INSERT INTO seats (id, hall_id, line, seat) VALUES (10, 2, 1, 1);
INSERT INTO seats (id, hall_id, line, seat) VALUES (11, 2, 1, 2);
INSERT INTO seats (id, hall_id, line, seat) VALUES (12, 2, 1, 3);
INSERT INTO seats (id, hall_id, line, seat) VALUES (13, 2, 1, 4);
INSERT INTO seats (id, hall_id, line, seat) VALUES (14, 2, 1, 5);

INSERT INTO seats (id, hall_id, line, seat) VALUES (15, 2, 2, 1);
INSERT INTO seats (id, hall_id, line, seat) VALUES (16, 2, 2, 2);
INSERT INTO seats (id, hall_id, line, seat) VALUES (17, 2, 2, 3);
INSERT INTO seats (id, hall_id, line, seat) VALUES (18, 2, 2, 4);
INSERT INTO seats (id, hall_id, line, seat) VALUES (19, 2, 2, 5);

-- Films
INSERT INTO films (id, name, age_restriction, premiere_datetime, duration, description) 
VALUES 
(1, 'Slider-Man: Reborn', 12, STR_TO_DATE('2022-01-01 06:30', '%Y-%m-%d %H:%i'), STR_TO_DATE('01:32', '%H:%i'), 'A movie about Slider-Man.');

INSERT INTO films (id, name, age_restriction, premiere_datetime, duration, description) 
VALUES 
(2, 'The Lord of the Pigs', 18, STR_TO_DATE('2022-01-04 12:00', '%Y-%m-%d %H:%i'), STR_TO_DATE('02:16', '%H:%i'), 'Great mighty pigs.');

INSERT INTO films (id, name, age_restriction, premiere_datetime, duration, description) 
VALUES 
(3, 'The Hammer', 21, STR_TO_DATE('2022-01-02 22:10', '%Y-%m-%d %H:%i'), STR_TO_DATE('01:48', '%H:%i'), 'A horror movie about the Hammer.');

-- Categories
INSERT INTO categories (id, name) VALUES (1, 'Adventure');
INSERT INTO categories (id, name) VALUES (2, 'Action');
INSERT INTO categories (id, name) VALUES (3, 'Comedy');
INSERT INTO categories (id, name) VALUES (4, 'Horror');
INSERT INTO categories (id, name) VALUES (5, 'Detective');

-- Film Category
-- Slider-Man - Action, Comedy
INSERT INTO film_has_category (film_id, category_id) VALUES (1, 2);
INSERT INTO film_has_category (film_id, category_id) VALUES (1, 3);

-- The Lord of the Pigs - Adventure
INSERT INTO film_has_category (film_id, category_id) VALUES (2, 1);

-- The Hammer - Horror, Detective
INSERT INTO film_has_category (film_id, category_id) VALUES (3, 4);
INSERT INTO film_has_category (film_id, category_id) VALUES (3, 5);

-- Screenings
-- Slider-Man 
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (1, 1, 1, STR_TO_DATE('2022-01-02 22:10', '%Y-%m-%d %H:%i'), 70);
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (2, 1, 1, STR_TO_DATE('2022-01-03 11:10', '%Y-%m-%d %H:%i'), 70);
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (3, 1, 2, STR_TO_DATE('2022-01-03 13:30', '%Y-%m-%d %H:%i'), 90);

-- The Lord of the Pigs
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (4, 2, 2, STR_TO_DATE('2022-01-04 12:00', '%Y-%m-%d %H:%i'), 110);
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (5, 2, 2, STR_TO_DATE('2022-01-04 15:10', '%Y-%m-%d %H:%i'), 110);
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (6, 2, 1, STR_TO_DATE('2022-01-04 17:30', '%Y-%m-%d %H:%i'), 90);

-- The Hammer
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (7, 3, 2, STR_TO_DATE('2022-01-02 22:10', '%Y-%m-%d %H:%i'), 130);
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (8, 3, 2, STR_TO_DATE('2022-01-03 11:10', '%Y-%m-%d %H:%i'), 100);
INSERT INTO screenings (id, film_id, hall_id, start_time, price) VALUES (9, 3, 1, STR_TO_DATE('2022-01-03 13:30', '%Y-%m-%d %H:%i'), 100);


-- Reservations
-- Order 1 - s1ckret
INSERT INTO reservations (id, seat_id, screening_id) VALUES (1, 4, 1);
INSERT INTO reservations (id, seat_id, screening_id) VALUES (2, 5, 1);
-- Order 2 - ivan
INSERT INTO reservations (id, seat_id, screening_id) VALUES (3, 8, 1);

-- Order 3 - yura
INSERT INTO reservations (id, seat_id, screening_id) VALUES (4, 4, 6);
INSERT INTO reservations (id, seat_id, screening_id) VALUES (5, 5, 6);
INSERT INTO reservations (id, seat_id, screening_id) VALUES (6, 6, 6);

-- Order 4 s1ckret
INSERT INTO reservations (id, seat_id, screening_id) VALUES (7, 16, 4);
INSERT INTO reservations (id, seat_id, screening_id) VALUES (8, 17, 4);
INSERT INTO reservations (id, seat_id, screening_id) VALUES (9, 18, 4);

-- Users
INSERT INTO users (id, email, username, password) VALUES (1, 's1ckret@gmail.com', 's1ckret', '$2a$12$GpP9dkrUn1.wwAXijjHDYOQwNDTL0Ez6ongr6KvwFE1y40zJC5mj.');
INSERT INTO users (id, email, username, password) VALUES (2, 'ivan@gmail.com', 'ivan', '$2a$12$12C.u6uz2/G.0iCvFg7Z1OjA6JKFqKnuIp6LTML81JqckciliJeoO');
INSERT INTO users (id, email, username, password) VALUES (3, 'yura@gmail.com', 'yura', '$2a$12$fOvDmx1NPJvuSwZGF.V83ufROCq81IjqRcRG6.JzYfZiIXhwlJR92');

-- Orders
INSERT INTO orders (id, user_id, create_time, total_price) VALUES (1, 1, STR_TO_DATE('2022-01-01 22:10', '%Y-%m-%d %H:%i'), 140);
INSERT INTO orders (id, user_id, create_time, total_price) VALUES (2, 2, STR_TO_DATE('2022-01-01 15:10', '%Y-%m-%d %H:%i'), 70);
INSERT INTO orders (id, user_id, create_time, total_price) VALUES (3, 3, STR_TO_DATE('2022-01-01 16:34', '%Y-%m-%d %H:%i'), 270);
INSERT INTO orders (id, user_id, create_time, total_price) VALUES (4, 1, STR_TO_DATE('2022-01-01 17:12', '%Y-%m-%d %H:%i'), 330);

-- Order has reservation
INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (1, 1);
INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (2, 1);

INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (3, 2);

INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (4, 3);
INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (5, 3);
INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (6, 3);

INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (7, 4);
INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (8, 4);
INSERT INTO order_has_reservation (reservation_id, order_id) VALUES (9, 4);
