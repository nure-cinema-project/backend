# backend

Backend server for nure-cinema-project

## How to deploy on minikube

### Starting minikube

If you have the next error:

```
❗  Unable to create dedicated network, this might result in cluster IP change after restart:
un-retryable: no free private network subnets found with given parameters (start: "192.168.9.0",
step: 9, tries: 20)
```

Then execute this command before starting minikube:

```sh
docker network create --subnet 192.168.9.0/24 --driver bridge minikube
```

In a terminal #1:

```sh
minikube start
minikube addons enable ingress
```

In a terminal #2:

```sh
minikube tunnel
```

### Deploying database and backend

In a terminal #1:

```sh
cd .kube

kubectl apply -f database/database-initdb-configmap.yml
kubectl apply -f database/database-pv.yaml
kubectl apply -f database/database-secret.yml
kubectl apply -f database/database-statefulset.yaml
kubectl apply -f database/database-service.yaml

kubectl apply -f backend/backend-configmap.yaml
kubectl apply -f backend/backend-deploymet.yaml
kubectl apply -f backend/backend-service.yaml
kubectl apply -f backend/backend-ingress-rule.yaml
```

Add a host domain to `/etc/hosts`:

```sh
echo | sudo tee -a /etc/hosts
echo "# The following lines are for NURE-Cinama minikube test" | sudo tee -a /etc/hosts
echo "$(minikube ip)      nure-cinema.ua" | sudo tee -a /etc/hosts
'

```

### Verifying deployment

Let's verify the deployment

1. Execute `kubectl get pod`
   It should look like this

```
NAME                                              READY   STATUS    RESTARTS   AGE
nure-cinema-backend-deployment-7f494b5d8d-66nnc   1/1     Running   0          18m
nure-cinema-database-0                            1/1     Running   0          26m
nure-cinema-database-1                            1/1     Running   0          25m
```

2. Execute `kubectl get sts`
   It should look like this

```
NAME                   READY   AGE
nure-cinema-database   2/2     31m
```

3. Execute `kubectl get cm`
   It should look like this

```
NAME                                 DATA   AGE
backend-configmap                    1      24m
kube-root-ca.crt                     1      32m
nure-cinema-database-initdb-config   1      32m
```

4. Execute `kubectl get svc`
   It should look like this

```
NAME                           TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)          AGE
kubernetes                     ClusterIP      10.96.0.1       <none>          443/TCP          33m
nure-cinema-backend-service    LoadBalancer   10.107.83.197   10.107.83.197   8080:32175/TCP   25m
nure-cinema-database-service   ClusterIP      None            <none>          3306/TCP         25m
```

5. Execute `kubectl get pv`
   It should look like this

```
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM                                                     STORAGECLASS   REASON   AGE
database-persistent-volume                 5Gi        RWO            Retain           Available                                                                                     33m
pvc-a26e8ff7-2453-46cf-9dc0-709dc71cc26c   1Gi        RWO            Delete           Bound       default/nure-cinema-database-pvc-nure-cinema-database-1   standard                33m
pvc-e9048e21-cfc0-4382-aad6-7da2d95bee86   1Gi        RWO            Delete           Bound       default/nure-cinema-database-pvc-nure-cinema-database-0   standard                33m
```

6. Execute `kubectl get ing`
   It should look like this

```
NAME                               CLASS   HOSTS            ADDRESS     PORTS   AGE
nure-cinema-backend-ingress-rule   nginx   nure-cinema.ua   localhost   80      24m
```

7. Navigate to `nure-cinema.ua/api/v1/halls` or `nure-cinema.ua/api/v1/films` in your browser

### Cleaning up

```sh
minikube stop
minikube remove
```
