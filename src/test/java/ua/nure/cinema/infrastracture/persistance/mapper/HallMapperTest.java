package ua.nure.cinema.infrastracture.persistance.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ua.nure.cinema.domain.model.hall.Hall;
import ua.nure.cinema.domain.model.hall.Seat;
import ua.nure.cinema.infrastracture.persistance.entity.HallEntity;
import ua.nure.cinema.infrastracture.persistance.entity.SeatEntity;

import java.util.List;

@SpringBootTest
class HallMapperTest {

    @Autowired
    private HallMapper hallMapper;

    @Test
    public void Given_HallEntities_When_MappedToDomain_Expect_DomainHalls() {
        List<HallEntity> entities = List.of(
                new HallEntity(1, "Red",
                        List.of(
                                new SeatEntity(1, 1, 1),
                                new SeatEntity(2, 1, 2)
                        )
                ),
                new HallEntity(2, "Blue",
                        List.of(
                                new SeatEntity(3, 1, 3),
                                new SeatEntity(4, 1, 4)
                        )
                )
        );

        List<Hall> expectedHalls = List.of(
                new Hall(1, "Red",
                        List.of(
                                new Seat(1, 1, 1),
                                new Seat(2, 1, 2)
                        )
                ),
                new Hall(2, "Blue",
                        List.of(
                                new Seat(3, 1, 3),
                                new Seat(4, 1, 4)
                        )
                )
        );

        List<Hall> realHalls = hallMapper.toModel(entities);

        Assertions.assertEquals(expectedHalls, realHalls);
    }
}
