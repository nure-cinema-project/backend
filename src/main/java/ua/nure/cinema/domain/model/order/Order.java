package ua.nure.cinema.domain.model.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ua.nure.cinema.domain.model.reservation.Reservation;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class Order {
    private long id;
    private LocalDateTime createTime;
    private List<Reservation> reservations;
    private long userId;
    private long totalPrice;
}
