package ua.nure.cinema.domain.model.screening;

import lombok.AllArgsConstructor;
import lombok.Data;
import ua.nure.cinema.domain.model.film.Film;
import ua.nure.cinema.domain.model.hall.Hall;
import ua.nure.cinema.domain.model.reservation.Reservation;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class Screening {
    private long id;
    private Film film;
    private Hall hall;
    private LocalDateTime startTime;
    private List<Reservation> reservations;
    private int price;
}
