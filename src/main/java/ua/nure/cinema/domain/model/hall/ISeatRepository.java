package ua.nure.cinema.domain.model.hall;

import java.util.List;

public interface ISeatRepository {
    List<Seat> findAll();
    Seat getById(Long id);
}
