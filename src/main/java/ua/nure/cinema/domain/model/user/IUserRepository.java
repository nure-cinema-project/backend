package ua.nure.cinema.domain.model.user;

import java.util.Optional;

public interface IUserRepository {
    User getById(Long id);

    User save(User user);

    Optional<User> findByEmail(String email);
}
