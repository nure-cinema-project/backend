package ua.nure.cinema.domain.model.screening;

import java.time.LocalDateTime;
import java.util.List;

public interface IScreeningRepository {
    List<Screening> findAll();
    Screening getById(Long id);
    List<Screening> findByStartTimeAfter(LocalDateTime time);
    List<Screening> findByFilmId(Long filid);
}
