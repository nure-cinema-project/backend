package ua.nure.cinema.domain.model.film;

import java.util.List;

public interface IFilmRepository {
    List<Film> findAll();
    Film getById(Long id);
}
