package ua.nure.cinema.domain.model.film;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Category {
    private long id;
    private String name;
}
