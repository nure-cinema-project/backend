package ua.nure.cinema.domain.model.hall;

import java.util.List;

public interface IHallRepository {
    List<Hall> findAll();
    Hall getById(Long id);
}
