package ua.nure.cinema.domain.model.hall;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Seat {
    private long id;
    private int line;
    private int seat;
}
