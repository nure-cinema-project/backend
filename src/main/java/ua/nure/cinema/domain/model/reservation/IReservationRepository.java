package ua.nure.cinema.domain.model.reservation;

import java.util.List;

public interface IReservationRepository {
    List<Reservation> findAll();
    Reservation getById(Long id);
}
