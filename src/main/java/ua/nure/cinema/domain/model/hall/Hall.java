package ua.nure.cinema.domain.model.hall;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Hall {
    private long id;
    private String name;
    private List<Seat> seats;
}
