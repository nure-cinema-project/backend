package ua.nure.cinema.domain.model.film;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class Film {
    private long id;
    private String name;
    private String description;
    private int ageRestriction;
    private LocalDateTime premiereDate;
    private LocalDateTime duration;
    private List<Category> categories;
}
