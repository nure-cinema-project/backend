package ua.nure.cinema.domain.model.reservation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ua.nure.cinema.domain.model.hall.Seat;
import ua.nure.cinema.domain.model.screening.Screening;

@Data
@Builder
@AllArgsConstructor
public class Reservation {
    private long id;
    private Seat seat;
    private Long screeningId;
}
