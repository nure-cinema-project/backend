package ua.nure.cinema.domain.model.order;

public interface IOrderRepository {
    Order save(Order order);
}
