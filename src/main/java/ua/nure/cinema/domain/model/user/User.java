package ua.nure.cinema.domain.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ua.nure.cinema.domain.model.order.Order;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class User {
    private long id;
    private String username;
    private String email;
    private String password;
    private List<Order> orders;
}
