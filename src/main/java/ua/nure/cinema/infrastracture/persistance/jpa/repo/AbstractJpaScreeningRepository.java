package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.infrastracture.persistance.entity.ScreeningEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AbstractJpaScreeningRepository extends JpaRepository<ScreeningEntity, Long> {
    List<ScreeningEntity> findByStartTimeAfter(LocalDateTime time);
    List<ScreeningEntity> findByFilm_Id(Long filid);
}
