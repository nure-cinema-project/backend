package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;
import ua.nure.cinema.domain.model.order.Order;
import ua.nure.cinema.infrastracture.persistance.entity.OrderEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = InnerSeatMapper.class)
public interface InnerOrderMapper {
    Order toModel(OrderEntity orderEntity, @Context CycleAvoidingMappingContext context);

    List<Order> toModel(List<OrderEntity> orderEntity, @Context CycleAvoidingMappingContext context);

    OrderEntity toEntity(Order order, @Context CycleAvoidingMappingContext context);

    List<OrderEntity> toEntity(List<Order> order, @Context CycleAvoidingMappingContext context);
}
