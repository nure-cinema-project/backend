package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.reservation.Reservation;
import ua.nure.cinema.domain.model.reservation.IReservationRepository;
import ua.nure.cinema.infrastracture.persistance.mapper.ReservationMapper;

import java.util.List;

@Repository
public class JpaReservationRepository implements IReservationRepository {
    private final AbstractJpaReservationRepository reservationRepository;
    private final ReservationMapper reservationMapper;

    @Autowired
    public JpaReservationRepository(AbstractJpaReservationRepository reservationRepository, ReservationMapper reservationMapper) {
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
    }

    @Override
    public List<Reservation> findAll() {
        return reservationMapper.toModel(reservationRepository.findAll());
    }

    @Override
    public Reservation getById(Long id) {
        return reservationMapper.toModel(reservationRepository.getById(id));
    }
}
