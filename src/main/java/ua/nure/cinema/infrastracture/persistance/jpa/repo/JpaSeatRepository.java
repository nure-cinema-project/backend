package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.hall.ISeatRepository;
import ua.nure.cinema.domain.model.hall.Seat;
import ua.nure.cinema.infrastracture.persistance.mapper.SeatMapper;

import java.util.List;

@Repository
public class JpaSeatRepository implements ISeatRepository {
    private final AbstractJpaSeatRepository seatRepository;
    private final SeatMapper seatMapper;

    @Autowired
    public JpaSeatRepository(AbstractJpaSeatRepository seatRepository, SeatMapper seatMapper) {
        this.seatRepository = seatRepository;
        this.seatMapper = seatMapper;
    }

    @Override
    public List<Seat> findAll() {
        return seatMapper.toModel(seatRepository.findAll());
    }

    @Override
    public Seat getById(Long id) {
        return seatMapper.toModel(seatRepository.getById(id));
    }
}
