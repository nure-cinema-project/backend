package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.order.Order;
import ua.nure.cinema.infrastracture.persistance.entity.OrderEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerOrderMapper;

import java.util.List;

@Component
public class OrderMapper {
    private final InnerOrderMapper mapper;

    @Autowired
    public OrderMapper(InnerOrderMapper mapper) {
        this.mapper = mapper;
    }

    public Order toModel(OrderEntity orderEntity) {
        return mapper.toModel(orderEntity, new CycleAvoidingMappingContext());
    }

    public List<Order> toModel(List<OrderEntity> orderEntity) {
        return mapper.toModel(orderEntity, new CycleAvoidingMappingContext());
    }

    public OrderEntity toEntity(Order order) {
        return mapper.toEntity(order, new CycleAvoidingMappingContext());
    }

    public List<OrderEntity> toEntity(List<Order> order) {
        return mapper.toEntity(order, new CycleAvoidingMappingContext());
    }
}
