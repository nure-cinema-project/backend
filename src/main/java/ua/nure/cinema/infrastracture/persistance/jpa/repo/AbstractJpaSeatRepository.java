package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.infrastracture.persistance.entity.SeatEntity;

@Repository
public interface AbstractJpaSeatRepository extends JpaRepository<SeatEntity, Long> {
}
