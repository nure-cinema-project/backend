package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ua.nure.cinema.domain.model.user.User;
import ua.nure.cinema.infrastracture.persistance.entity.UserEntity;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = {InnerOrderMapper.class})
public interface InnerUserMapper {
    User toModel(UserEntity userEntity, @Context CycleAvoidingMappingContext context);

    UserEntity toEntity(User user, @Context CycleAvoidingMappingContext context);
}
