package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;
import ua.nure.cinema.domain.model.reservation.Reservation;
import ua.nure.cinema.infrastracture.persistance.entity.ReservationEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = {InnerScreeningMapper.class, InnerSeatMapper.class})
public interface InnerReservationMapper {
    Reservation toModel(ReservationEntity reservationEntity, @Context CycleAvoidingMappingContext context);

    List<Reservation> toModel(List<ReservationEntity> reservationEntity, @Context CycleAvoidingMappingContext context);

    ReservationEntity toEntity(Reservation reservation, @Context CycleAvoidingMappingContext context);

    List<ReservationEntity> toEntity(List<Reservation> reservation, @Context CycleAvoidingMappingContext context);
}
