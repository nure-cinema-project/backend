package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Halls")
public class HallEntity {
    @Id @GeneratedValue
    @SequenceGenerator(name="hall_seq", sequenceName="hall_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "hallId")
    @EqualsAndHashCode.Exclude
    private List<SeatEntity> seats;
}
