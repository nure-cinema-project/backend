package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.film.Film;
import ua.nure.cinema.domain.model.film.IFilmRepository;
import ua.nure.cinema.infrastracture.persistance.mapper.FilmMapper;

import java.util.List;

@Repository
public class JpaFilmRepository implements IFilmRepository {
    private final AbstractJpaFilmRepository filmRepository;
    private final FilmMapper filmMapper;

    @Autowired
    public JpaFilmRepository(AbstractJpaFilmRepository filmRepository, FilmMapper filmMapper) {
        this.filmRepository = filmRepository;
        this.filmMapper = filmMapper;
    }

    @Override
    public List<Film> findAll() {
        return filmMapper.toModel(filmRepository.findAll());
    }

    @Override
    public Film getById(Long id) {
        return filmMapper.toModel(filmRepository.getById(id));
    }
}
