package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.film.Film;
import ua.nure.cinema.infrastracture.persistance.entity.FilmEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerFilmMapper;

import java.util.List;

@Component
public class FilmMapper {
    private final InnerFilmMapper mapper;

    @Autowired
    public FilmMapper(InnerFilmMapper mapper) {
        this.mapper = mapper;
    }

    public Film toModel(FilmEntity filmEntity) {
        return mapper.toModel(filmEntity, new CycleAvoidingMappingContext());
    }

    public List<Film> toModel(List<FilmEntity> filmEntity) {
        return mapper.toModel(filmEntity, new CycleAvoidingMappingContext());
    }

    public FilmEntity toEntity(Film film) {
        return mapper.toEntity(film, new CycleAvoidingMappingContext());
    }

    public List<FilmEntity> toEntity(List<Film> film) {
        return mapper.toEntity(film, new CycleAvoidingMappingContext());
    }
}
