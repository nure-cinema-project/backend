package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Seats")
public class SeatEntity {
    @Id @GeneratedValue
    @SequenceGenerator(name="seat_seq", sequenceName="seat_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "line")
    private int line;

    @Column(name = "seat")
    private int seat;
}
