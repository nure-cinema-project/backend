package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.hall.Hall;
import ua.nure.cinema.domain.model.hall.IHallRepository;
import ua.nure.cinema.infrastracture.persistance.mapper.HallMapper;

import java.util.List;

@Repository
public class JpaHallRepository implements IHallRepository {
    private final AbstractJpaHallRepository hallRepository;
    private final HallMapper hallMapper;

    @Autowired
    public JpaHallRepository(AbstractJpaHallRepository hallRepository, HallMapper hallMapper) {
        this.hallRepository = hallRepository;
        this.hallMapper = hallMapper;
    }

    @Override
    public List<Hall> findAll() {
        return hallMapper.toModel(hallRepository.findAll());
    }

    @Override
    public Hall getById(Long id) {
        return hallMapper.toModel(hallRepository.getById(id));
    }
}
