package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Films")
public class FilmEntity {
    @Id @GeneratedValue
    @SequenceGenerator(name="film_seq", sequenceName="film_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    @Lob
    private String description;

    @Column(name = "ageRestriction")
    private int ageRestriction;

    @Column(name = "premiereDatetime")
    private LocalDateTime premiereDate;

    @Column(name = "duration")
    private LocalDateTime duration;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(
            name = "FilmHasCategory",
            joinColumns = { @JoinColumn(name = "filmId") },
            inverseJoinColumns = { @JoinColumn(name = "categoryId") }
    )
    @EqualsAndHashCode.Exclude
    private List<CategoryEntity> categories;
}
