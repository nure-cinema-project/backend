package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.order.IOrderRepository;
import ua.nure.cinema.domain.model.order.Order;
import ua.nure.cinema.domain.model.user.User;
import ua.nure.cinema.infrastracture.persistance.mapper.OrderMapper;

@Repository
public class JpaOrderRepository implements IOrderRepository {
    private final AbstractJpaOrderRepository orderRepository;
    private final OrderMapper orderMapper;

    @Autowired
    public JpaOrderRepository(AbstractJpaOrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Override
    public Order save(Order order) {
        return orderMapper.toModel(orderRepository.save(orderMapper.toEntity(order)));
    }
}
