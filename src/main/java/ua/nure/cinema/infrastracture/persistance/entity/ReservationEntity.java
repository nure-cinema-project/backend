package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Reservations")
public class ReservationEntity {
    @Id @GeneratedValue
    @SequenceGenerator(name="reservation_seq", sequenceName="reservation_seq")
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "seatId")
    private SeatEntity seat;

    @Column(name = "screeningId", updatable = false)
    private Long screeningId;
}
