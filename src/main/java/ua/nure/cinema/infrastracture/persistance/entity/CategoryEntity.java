package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="Categories")
public class CategoryEntity {
    @Id @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;
}
