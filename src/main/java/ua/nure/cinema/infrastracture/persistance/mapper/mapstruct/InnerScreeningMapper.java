package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ua.nure.cinema.domain.model.screening.Screening;
import ua.nure.cinema.infrastracture.persistance.entity.ScreeningEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = {InnerFilmMapper.class, InnerReservationMapper.class, InnerHallMapper.class})
public interface InnerScreeningMapper {
    Screening toModel(ScreeningEntity screeningEntity,
                      @Context CycleAvoidingMappingContext context);

    List<Screening> toModel(List<ScreeningEntity> screeningEntity,
                            @Context CycleAvoidingMappingContext context);

    ScreeningEntity toEntity(Screening screening, @Context CycleAvoidingMappingContext context);

    List<ScreeningEntity> toEntity(List<Screening> screening,
                                   @Context CycleAvoidingMappingContext context);
}
