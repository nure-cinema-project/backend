package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;
import ua.nure.cinema.domain.model.hall.Seat;
import ua.nure.cinema.infrastracture.persistance.entity.SeatEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface InnerSeatMapper {
    Seat toModel(SeatEntity seatEntity, @Context CycleAvoidingMappingContext context);

    List<Seat> toModel(List<SeatEntity> seatEntity, @Context CycleAvoidingMappingContext context);

    SeatEntity toEntity(Seat seat, @Context CycleAvoidingMappingContext context);

    List<SeatEntity> toEntity(List<Seat> seat, @Context CycleAvoidingMappingContext context);
}
