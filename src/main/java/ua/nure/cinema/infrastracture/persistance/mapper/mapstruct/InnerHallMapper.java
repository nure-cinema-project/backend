package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;
import ua.nure.cinema.domain.model.hall.Hall;
import ua.nure.cinema.infrastracture.persistance.entity.HallEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = InnerSeatMapper.class)
public interface InnerHallMapper {
    Hall toModel(HallEntity hallEntity, @Context CycleAvoidingMappingContext context);

    List<Hall> toModel(List<HallEntity> hallEntity, @Context CycleAvoidingMappingContext context);

    HallEntity toEntity(Hall hall, @Context CycleAvoidingMappingContext context);

    List<HallEntity> toEntity(List<Hall> hall, @Context CycleAvoidingMappingContext context);
}
