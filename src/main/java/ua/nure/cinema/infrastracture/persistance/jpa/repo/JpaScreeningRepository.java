package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.screening.Screening;
import ua.nure.cinema.domain.model.screening.IScreeningRepository;
import ua.nure.cinema.infrastracture.persistance.mapper.ScreeningMapper;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class JpaScreeningRepository implements IScreeningRepository {
    private final AbstractJpaScreeningRepository screeningRepository;
    private final ScreeningMapper screeningMapper;

    @Autowired
    public JpaScreeningRepository(AbstractJpaScreeningRepository screeningRepository, ScreeningMapper screeningMapper) {
        this.screeningRepository = screeningRepository;
        this.screeningMapper = screeningMapper;
    }

    @Override
    public List<Screening> findAll() {
        return screeningMapper.toModel(screeningRepository.findAll());
    }

    @Override
    public Screening getById(Long id) {
        return screeningMapper.toModel(screeningRepository.getById(id));
    }

    @Override
    public List<Screening> findByStartTimeAfter(LocalDateTime time) {
        return screeningMapper.toModel(screeningRepository.findByStartTimeAfter(time));
    }

    @Override
    public List<Screening> findByFilmId(Long filid) {
        return screeningMapper.toModel(screeningRepository.findByFilm_Id(filid));
    }
}
