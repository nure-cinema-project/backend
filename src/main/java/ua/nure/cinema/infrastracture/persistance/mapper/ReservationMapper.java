package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.reservation.Reservation;
import ua.nure.cinema.infrastracture.persistance.entity.ReservationEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerReservationMapper;

import java.util.List;

@Component
public class ReservationMapper {
    private final InnerReservationMapper mapper;

    @Autowired
    public ReservationMapper(InnerReservationMapper mapper) {
        this.mapper = mapper;
    }

    public Reservation toModel(ReservationEntity reservationEntity) {
        return mapper.toModel(reservationEntity, new CycleAvoidingMappingContext());
    }

    public List<Reservation> toModel(List<ReservationEntity> reservationEntity) {
        return mapper.toModel(reservationEntity, new CycleAvoidingMappingContext());
    }

    public ReservationEntity toEntity(Reservation reservation) {
        return mapper.toEntity(reservation, new CycleAvoidingMappingContext());
    }

    public List<ReservationEntity> toEntity(List<Reservation> reservation) {
        return mapper.toEntity(reservation, new CycleAvoidingMappingContext());
    }
}
