package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.user.User;
import ua.nure.cinema.infrastracture.persistance.entity.UserEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerUserMapper;

@Component
public class UserMapper {
    private final InnerUserMapper mapper;

    @Autowired
    public UserMapper(InnerUserMapper mapper) {
        this.mapper = mapper;
    }

    public User toModel(UserEntity userEntity) {
        return mapper.toModel(userEntity, new CycleAvoidingMappingContext());
    }

    public UserEntity toEntity(User user) {
        return mapper.toEntity(user, new CycleAvoidingMappingContext());
    }
}
