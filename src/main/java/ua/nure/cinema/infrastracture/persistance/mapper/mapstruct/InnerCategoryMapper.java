package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ua.nure.cinema.domain.model.film.Category;
import ua.nure.cinema.infrastracture.persistance.entity.CategoryEntity;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface InnerCategoryMapper {
    Category toModel(CategoryEntity categoryEntity, @Context CycleAvoidingMappingContext context);

    CategoryEntity toEntity(Category category, @Context CycleAvoidingMappingContext context);
}
