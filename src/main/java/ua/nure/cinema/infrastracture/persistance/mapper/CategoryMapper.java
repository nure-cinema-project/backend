package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.film.Category;
import ua.nure.cinema.infrastracture.persistance.entity.CategoryEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerCategoryMapper;

@Component
public class CategoryMapper {
    private final InnerCategoryMapper mapper;

    @Autowired
    public CategoryMapper(InnerCategoryMapper mapper) {
        this.mapper = mapper;
    }

    public Category toModel(CategoryEntity categoryEntity) {
        return mapper.toModel(categoryEntity, new CycleAvoidingMappingContext());
    }

    public CategoryEntity toEntity(Category category) {
        return mapper.toEntity(category, new CycleAvoidingMappingContext());
    }
}
