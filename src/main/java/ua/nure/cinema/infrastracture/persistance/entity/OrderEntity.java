package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Orders")
public class OrderEntity {
    @Id @GeneratedValue
    @SequenceGenerator(name="order_seq", sequenceName="order_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "createTime")
    private LocalDateTime createTime;

    @Column(name = "userId", updatable = false)
    private long userId;

    @Column(name = "totalPrice")
    private long totalPrice;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "OrderHasReservation",
            joinColumns = { @JoinColumn(name = "orderId") },
            inverseJoinColumns = { @JoinColumn(name = "reservationId") }
    )
    @EqualsAndHashCode.Exclude
    private List<ReservationEntity> reservations;
}
