package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.domain.model.user.IUserRepository;
import ua.nure.cinema.domain.model.user.User;
import ua.nure.cinema.infrastracture.persistance.mapper.UserMapper;

import java.util.Optional;

@Repository
public class JpaUserRepository implements IUserRepository {
    private final AbstractJpaUserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public JpaUserRepository(AbstractJpaUserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public User getById(Long id) {
        return userMapper.toModel(userRepository.getById(id));
    }

    @Override
    public User save(User user) {
        return userMapper.toModel(userRepository.saveAndFlush(userMapper.toEntity(user)));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email).map(userMapper::toModel);
    }
}
