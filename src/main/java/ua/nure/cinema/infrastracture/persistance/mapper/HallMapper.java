package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.hall.Hall;
import ua.nure.cinema.infrastracture.persistance.entity.HallEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerHallMapper;

import java.util.List;

@Component
public class HallMapper {
    private final InnerHallMapper mapper;

    @Autowired
    public HallMapper(InnerHallMapper mapper) {
        this.mapper = mapper;
    }

    public Hall toModel(HallEntity hallEntity) {
        return mapper.toModel(hallEntity, new CycleAvoidingMappingContext());
    }

    public List<Hall> toModel(List<HallEntity> hallEntity) {
        return mapper.toModel(hallEntity, new CycleAvoidingMappingContext());
    }

    public HallEntity toEntity(Hall hall) {
        return mapper.toEntity(hall, new CycleAvoidingMappingContext());
    }

    public List<HallEntity> toEntity(List<Hall> hall) {
        return mapper.toEntity(hall, new CycleAvoidingMappingContext());
    }
}
