package ua.nure.cinema.infrastracture.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Screenings")
public class ScreeningEntity {
    @Id @GeneratedValue
    @SequenceGenerator(name="screening_seq", sequenceName="screening_seq")
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "filmId")
    private FilmEntity film;

    @ManyToOne
    @JoinColumn(name = "hallId")
    private HallEntity hall;

    @OneToMany
    @JoinColumn(name = "screeningId")
    @EqualsAndHashCode.Exclude
    private List<ReservationEntity> reservations;

    @Column(name = "startTime")
    private LocalDateTime startTime;

    @Column(name = "price")
    private int price;
}
