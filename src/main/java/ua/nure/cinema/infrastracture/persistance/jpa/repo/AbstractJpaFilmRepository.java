package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.infrastracture.persistance.entity.FilmEntity;

@Repository
public interface AbstractJpaFilmRepository extends JpaRepository<FilmEntity, Long> {
}
