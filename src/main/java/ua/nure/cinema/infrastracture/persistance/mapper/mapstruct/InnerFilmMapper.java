package ua.nure.cinema.infrastracture.persistance.mapper.mapstruct;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ua.nure.cinema.domain.model.film.Film;
import ua.nure.cinema.infrastracture.persistance.entity.FilmEntity;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = InnerCategoryMapper.class)
public interface InnerFilmMapper {
    Film toModel(FilmEntity filmEntity, @Context CycleAvoidingMappingContext context);

    List<Film> toModel(List<FilmEntity> filmEntity, @Context CycleAvoidingMappingContext context);

    FilmEntity toEntity(Film film, @Context CycleAvoidingMappingContext context);

    List<FilmEntity> toEntity(List<Film> film, @Context CycleAvoidingMappingContext context);
}
