package ua.nure.cinema.infrastracture.persistance.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.cinema.infrastracture.persistance.entity.UserEntity;

import java.util.Optional;

@Repository
public interface AbstractJpaUserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByEmail(String email);
}
