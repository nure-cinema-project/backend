package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.hall.Seat;
import ua.nure.cinema.infrastracture.persistance.entity.SeatEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerSeatMapper;

import java.util.List;

@Component
public class SeatMapper {
    private final InnerSeatMapper mapper;

    @Autowired
    public SeatMapper(InnerSeatMapper mapper) {
        this.mapper = mapper;
    }

    public Seat toModel(SeatEntity seatEntity) {
        return mapper.toModel(seatEntity, new CycleAvoidingMappingContext());
    }

    public List<Seat> toModel(List<SeatEntity> seatEntity) {
        return mapper.toModel(seatEntity, new CycleAvoidingMappingContext());
    }

    public SeatEntity toEntity(Seat seat) {
        return mapper.toEntity(seat, new CycleAvoidingMappingContext());
    }

    public List<SeatEntity> toEntity(List<Seat> seat) {
        return mapper.toEntity(seat, new CycleAvoidingMappingContext());
    }
}
