package ua.nure.cinema.infrastracture.persistance.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.nure.cinema.domain.model.screening.Screening;
import ua.nure.cinema.infrastracture.persistance.entity.ScreeningEntity;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.CycleAvoidingMappingContext;
import ua.nure.cinema.infrastracture.persistance.mapper.mapstruct.InnerScreeningMapper;

import java.util.List;

@Component
public class ScreeningMapper {
    private final InnerScreeningMapper mapper;

    @Autowired
    public ScreeningMapper(InnerScreeningMapper mapper) {
        this.mapper = mapper;
    }

    public Screening toModel(ScreeningEntity screeningEntity) {
        return mapper.toModel(screeningEntity, new CycleAvoidingMappingContext());
    }

    public List<Screening> toModel(List<ScreeningEntity> screeningEntity) {
        return mapper.toModel(screeningEntity, new CycleAvoidingMappingContext());
    }

    public ScreeningEntity toEntity(Screening screening) {
        return mapper.toEntity(screening, new CycleAvoidingMappingContext());
    }

    public List<ScreeningEntity> toEntity(List<Screening> screening) {
        return mapper.toEntity(screening, new CycleAvoidingMappingContext());
    }
}
