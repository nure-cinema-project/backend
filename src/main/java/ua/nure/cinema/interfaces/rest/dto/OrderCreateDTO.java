package ua.nure.cinema.interfaces.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class OrderCreateDTO {
    private Long screeningId;
    private List<Long> seatIds;
}
