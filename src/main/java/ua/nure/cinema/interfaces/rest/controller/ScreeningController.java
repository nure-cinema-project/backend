package ua.nure.cinema.interfaces.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.cinema.domain.model.screening.IScreeningRepository;
import ua.nure.cinema.domain.model.screening.Screening;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ScreeningController {

    private final IScreeningRepository screeningRepository;

    @Autowired
    public ScreeningController(IScreeningRepository screeningRepository) {
        this.screeningRepository = screeningRepository;
    }

    @GetMapping("/screenings")
    @ResponseBody
    public List<Screening> getScreenings() {
        return screeningRepository.findAll();
    }

    @GetMapping(value = "/screenings", params = "dateTime")
    @ResponseBody
    public List<Screening> getScreeningsAfterStartDateTime(@RequestParam LocalDateTime dateTime) {
        return screeningRepository.findByStartTimeAfter(dateTime);
    }

    @GetMapping(value = "/screenings", params = "filmId")
    @ResponseBody
    public List<Screening> getScreeningsByFilm(@RequestParam Long filmId) {
        return screeningRepository.findByFilmId(filmId);
    }

    @GetMapping("/screenings/{screeningId}")
    @ResponseBody
    public Screening getScreening(@PathVariable Long screeningId) {
        return screeningRepository.getById(screeningId);
    }
}
