package ua.nure.cinema.interfaces.rest.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class EmailTicketDto {
    private String filmName;
    private String hallName;
    private LocalDateTime time;
    private int seat;
    private int line;
    private int price;
}
