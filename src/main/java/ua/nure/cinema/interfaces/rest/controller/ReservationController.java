package ua.nure.cinema.interfaces.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.cinema.domain.model.reservation.Reservation;
import ua.nure.cinema.domain.model.reservation.IReservationRepository;
import ua.nure.cinema.domain.model.reservation.Reservation;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ReservationController {

    private final IReservationRepository reservationRepository;

    @Autowired
    public ReservationController(IReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @GetMapping("/reservations")
    @ResponseBody
    public List<Reservation> getReservations() {
        return reservationRepository.findAll();
    }

    @GetMapping("/reservations/{reservationId}")
    @ResponseBody
    public Reservation getReservation(@PathVariable Long reservationId) {
        return reservationRepository.getById(reservationId);
    }

}
