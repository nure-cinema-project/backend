package ua.nure.cinema.interfaces.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ua.nure.cinema.application.EmailServiceImpl;
import ua.nure.cinema.domain.model.hall.ISeatRepository;
import ua.nure.cinema.domain.model.hall.Seat;
import ua.nure.cinema.domain.model.order.IOrderRepository;
import ua.nure.cinema.domain.model.order.Order;
import ua.nure.cinema.domain.model.reservation.IReservationRepository;
import ua.nure.cinema.domain.model.reservation.Reservation;
import ua.nure.cinema.domain.model.screening.IScreeningRepository;
import ua.nure.cinema.domain.model.screening.Screening;
import ua.nure.cinema.domain.model.user.IUserRepository;
import ua.nure.cinema.domain.model.user.User;
import ua.nure.cinema.interfaces.rest.dto.EmailTicketDto;
import ua.nure.cinema.interfaces.rest.dto.OrderCreateDTO;
import ua.nure.cinema.interfaces.rest.dto.TicketDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    private final IReservationRepository reservationRepository;
    private final IUserRepository userRepository;
    private final IScreeningRepository screeningRepository;
    private final ISeatRepository seatRepository;
    private final IOrderRepository orderRepository;
    private final EmailServiceImpl emailService;

    @Autowired
    public UserController(IReservationRepository reservationRepository, IUserRepository userRepository, IScreeningRepository screeningRepository, ISeatRepository seatRepository, IOrderRepository orderRepository, EmailServiceImpl emailService) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.screeningRepository = screeningRepository;
        this.seatRepository = seatRepository;
        this.orderRepository = orderRepository;
        this.emailService = emailService;
    }

    @GetMapping("/user/tickets")
    @ResponseBody
    public List<TicketDto> getTickets(Authentication authentication) {
        String email = ((UserDetails) authentication.getPrincipal()).getUsername();
        Optional<User> optionalUser = userRepository.findByEmail(email);

        List<TicketDto> tickets = new ArrayList<>();
        optionalUser.ifPresent(user -> {
            user.getOrders().forEach(order -> {
                order.getReservations().forEach(reservation -> {
                    Screening screening = screeningRepository.getById(reservation.getScreeningId());
                    TicketDto ticketDto = TicketDto.builder()
                            .number(reservation.getId())
                            .price(screening.getPrice())
                            .time(screening.getStartTime())
                            .filmName(screening.getFilm().getName())
                            .hallName(screening.getHall().getName())
                            .line(reservation.getSeat().getLine())
                            .seat(reservation.getSeat().getSeat())
                            .build();
                    tickets.add(ticketDto);
                });
            });
        });

        return tickets;
    }

    @PostMapping("/user/orders")
    public Order createOrder(Authentication authentication, @RequestBody OrderCreateDTO orderCreateDTO) {
        String email = ((UserDetails) authentication.getPrincipal()).getUsername();
        Optional<User> optionalUser = userRepository.findByEmail(email);

        List<Reservation> reservations = new ArrayList<>();
        AtomicLong totalPrice = new AtomicLong();
        Order.OrderBuilder orderBuilder = Order.builder();
        optionalUser.ifPresent(user -> {
            List<EmailTicketDto> emailTickets = new ArrayList<>();

            Screening screening = screeningRepository.getById(orderCreateDTO.getScreeningId());
            orderCreateDTO.getSeatIds().forEach(id -> {
                Seat seat = seatRepository.getById(id);
                Reservation reservation = Reservation.builder()
                        .screeningId(orderCreateDTO.getScreeningId())
                        .seat(seat)
                        .build();
                reservations.add(reservation);
                totalPrice.addAndGet(screening.getPrice());

                emailTickets.add(EmailTicketDto.builder()
                        .time(LocalDateTime.now())
                        .filmName(screening.getFilm().getName())
                        .price(screening.getPrice())
                        .hallName(screening.getHall().getName())
                        .seat(seat.getSeat())
                        .line(seat.getLine())
                        .build());
            });

            orderBuilder
                    .totalPrice(totalPrice.get())
                    .userId(user.getId())
                    .createTime(LocalDateTime.now())
                    .reservations(reservations);

            orderRepository.save(orderBuilder.build());

            emailService.sendTickets(user.getUsername(), user.getEmail(), emailTickets);
        });

        return orderBuilder.build();
    }
}
