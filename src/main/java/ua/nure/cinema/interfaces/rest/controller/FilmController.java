package ua.nure.cinema.interfaces.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.cinema.domain.model.film.Film;
import ua.nure.cinema.domain.model.film.IFilmRepository;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class FilmController {

    private final IFilmRepository filmRepository;

    @Autowired
    public FilmController(IFilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @GetMapping("/films")
    @ResponseBody
    public List<Film> getFilms() {
        return filmRepository.findAll();
    }

    @GetMapping("/films/{filmId}")
    @ResponseBody
    public Film getFilm(@PathVariable Long filmId) {
        return filmRepository.getById(filmId);
    }
}
