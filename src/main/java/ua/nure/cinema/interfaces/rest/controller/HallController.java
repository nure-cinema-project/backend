package ua.nure.cinema.interfaces.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.cinema.domain.model.hall.Hall;
import ua.nure.cinema.domain.model.hall.IHallRepository;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class HallController {

    private final IHallRepository hallRepository;

    @Autowired
    public HallController(IHallRepository hallRepository) {
        this.hallRepository = hallRepository;
    }

    @GetMapping("/halls")
    @ResponseBody
    public List<Hall> getHalls() {
        return hallRepository.findAll();
    }

    @GetMapping("/halls/{hallId}")
    @ResponseBody
    public Hall getHall(@PathVariable Long hallId) {
        return hallRepository.getById(hallId);
    }
}
