package ua.nure.cinema.interfaces.rest.dto;

import lombok.Data;

@Data
public class RegistrationRequestDTO {
    private String email;
    private String username;
    private String password;
}
