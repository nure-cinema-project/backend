package ua.nure.cinema.interfaces.rest.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class TicketDto {
    private long number;
    private String filmName;
    private String hallName;
    private LocalDateTime time;
    private int seat;
    private int line;
    private int price;
}
