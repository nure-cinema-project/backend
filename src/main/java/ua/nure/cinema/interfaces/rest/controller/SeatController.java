package ua.nure.cinema.interfaces.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.cinema.domain.model.hall.ISeatRepository;
import ua.nure.cinema.domain.model.hall.Seat;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class SeatController {

    private final ISeatRepository seatRepository;

    @Autowired
    public SeatController(ISeatRepository seatRepository) {
        this.seatRepository = seatRepository;
    }

    @GetMapping("/seats")
    @ResponseBody
    public List<Seat> getSeats() {
        return seatRepository.findAll();
    }

    @GetMapping("/seats/{seatId}")
    @ResponseBody
    public Seat getSeat(@PathVariable Long seatId) {
        return seatRepository.getById(seatId);
    }
}
