package ua.nure.cinema.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.nayuki.qrcodegen.QrCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import ua.nure.cinema.interfaces.rest.dto.EmailTicketDto;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public class EmailServiceImpl {
    private final JavaMailSenderImpl emailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSenderImpl emailSender) {
        this.emailSender = emailSender;
    }

    public void sendSimpleMessage(
            String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("nure-cinema-noreply@nure.ua");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    public void sendTickets(String username, String email, List<EmailTicketDto> tickets) {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        String jsonTickets = "";
        try {
            jsonTickets = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tickets);
        } catch (JsonProcessingException e) {
            jsonTickets = "error!";
            e.printStackTrace();
        }

        QrCode qrCodeTickets = QrCode.encodeText(jsonTickets, QrCode.Ecc.LOW);
        BufferedImage imageTickets = qrCodeTickets.toImage(8, 4);

        MimeMessage message = emailSender.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
            messageHelper.setTo(email);
            messageHelper.setSubject("Nure Cinema");
            messageHelper.setText("<h1>Hello, " + username + "! Here is your tickets: </h1>", true);

            File qrCodeTempFile = File.createTempFile("qrCode-", ".png");
            ImageIO.write(imageTickets, "png", qrCodeTempFile);
            messageHelper.addInline("ticket-qr-code", qrCodeTempFile);

        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
        emailSender.send(message);
    }
}
