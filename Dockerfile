FROM gradle:7.2.0-jdk11-alpine AS build
WORKDIR /usr/app

COPY build.gradle settings.gradle ./
COPY src ./src

RUN gradle bootJar

FROM eclipse-temurin:11-jre-alpine AS image
WORKDIR /usr/app

COPY --from=build /usr/app/build/libs/*.jar ./spring-boot-application.jar

EXPOSE 8080

# TODO: JRE memory configuration
ENTRYPOINT ["java", "-jar", "spring-boot-application.jar"]
